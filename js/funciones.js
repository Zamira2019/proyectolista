let tareas = document.getElementById('tareas'); tareas

let tarTotal = document.getElementById('tarTotal');
let tarFin = document.getElementById('tarFin');
let tarFal = document.getElementById('tarFal');
let tareaIns = document.getElementById('tareaIns');


function guardar() {
    if (tareaIns.value.trim() != "") {
        agregarTarea();
        actualizarDatos();
    }
}

tareas.addEventListener('click',function(event){

    if(event.target.textContent === 'X'){
        event.target.parentElement.parentElement.remove();
    }

    actualizarDatos();
})

function agregarTarea() {
    let li = document.createElement('li');
    li.classList.add('tarea');
    li.innerHTML = `
                    <div class="TareaN" id="TN">
                        <input type="checkbox" ><label>${tareaIns.value}</label>
                        <button type="button" class="borrar" onclick="borrarT">X</button>
                    </div>
                    `;

        tareas.appendChild(li);

    tareaIns.value = '';
}


function actualizarDatos() {

    let tareasFinalNum = 0;
    let tareasNum = 0;

    for (let x = 0; x < document.getElementsByClassName('TareaN').length; x++) {
        if (document.getElementsByClassName('TareaN')[x].children[0].checked) {
            tareasFinalNum += 1;
        }
    }

    tareasNum = document.getElementsByClassName('TareaN').length;

    tarTotal.innerHTML = tareasNum;
    tarFin.innerHTML = tareasFinalNum;
    tarFal.innerHTML = tareasNum - tareasFinalNum;

    localStorage.setItem('tarTotalStor', tarTotal.innerHTML);
    localStorage.setItem('tarFin', tarFin.innerHTML);
    localStorage.setItem('tarFal', tarFal.innerHTML);
}


function guardarU(){
    let usu = document.getElementById("Usuario").value;
    sessionStorage.setItem("claveUsuario",usu);    
}
